package com.epam.esm.restbasicsboot.service;

import java.util.List;

import com.epam.esm.restbasicsboot.repository.entity.GiftCertificate;
import org.springframework.stereotype.Component;


/**
 * This interface imposes business logic 
 * on gift certificate class. 
 * @author yusuf
 */
@Component
public interface GiftCertManager {
	GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate);
	List<GiftCertificate> findAll();
	GiftCertificate findGiftCertificateById(long id);
	GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate);
	GiftCertificate deleteGiftCertificateById(long id);
	List<GiftCertificate> findSortedGiftCertificatesAndTagsByTagName(String tagName, String[] sortBy);
	List<GiftCertificate> findGiftCertificatesAndTagsByTagName(String tagName);
	List<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfName(String partOfName, String[] sortBy);
	List<GiftCertificate> findGiftCertificatesAndTagsByPartOfName(String partOfName);
	List<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, String[] sortBy);
	List<GiftCertificate> findGiftCertificatesAndTagsByPartOfDesc(String partOfDesc);
}
