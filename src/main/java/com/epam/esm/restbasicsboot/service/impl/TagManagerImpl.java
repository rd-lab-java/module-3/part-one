package com.epam.esm.restbasicsboot.service.impl;

import java.util.List;

import com.epam.esm.restbasicsboot.repository.TagRepository;
import com.epam.esm.restbasicsboot.repository.entity.Tag;
import com.epam.esm.restbasicsboot.service.TagManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Tag Service interface implementation that contains 
 * implementation of CRUD opertations business logics
 * 
 * @author yusuf
 *
 */
@Service
public class TagManagerImpl implements TagManager {
	
	@Autowired
	private TagRepository tagRepository;
	
	@Override
	@Transactional
	public Tag saveTag(Tag tag) {
		// TODO Auto-generated method stub
		return tagRepository.saveTag(tag);
	}

	@Override
	@Transactional
	public List<Tag> findTagsByGiftCertificateId(long giftCertId) {
		return tagRepository.findTagsByGiftCertificateId(giftCertId);
	}

	@Override
	@Transactional
	public List<Tag> findAll() {
		return tagRepository.findAll();
	}

	@Override
	@Transactional
	public Tag deleteTagById(long id) {
		return tagRepository.deleteTagById(id);
	}

	@Override
	@Transactional
	public Tag findTagById(long id) {
		return tagRepository.findTagById(id);
	}

}
