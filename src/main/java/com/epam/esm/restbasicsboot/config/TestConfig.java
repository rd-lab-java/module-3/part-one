package com.epam.esm.restbasicsboot.config;

import com.epam.esm.restbasicsboot.repository.GiftCertRepository;
import com.epam.esm.restbasicsboot.repository.TagRepository;
import com.epam.esm.restbasicsboot.repository.impl.GiftCertRepositoryImpl;
import com.epam.esm.restbasicsboot.repository.impl.TagRepositoryImpl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@PropertySources({
        @PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
        @PropertySource(name="stringValues", value = "classpath:string-values.properties")
})
public class TestConfig {

    @Bean
    @Profile("test")
    public DataSource getDevDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:schema.sql")
                .build();
    }

    @Bean
    @Profile("test")
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @Profile("test")
    public TagRepository getTagRepository(Environment environment, JdbcTemplate jdbcTemplate) {
        return new TagRepositoryImpl(jdbcTemplate, environment);
    }

    @Bean
    @Profile("test")
    public GiftCertRepository getGiftCertRepository(Environment environment, JdbcTemplate jdbcTemplate) {
        return new GiftCertRepositoryImpl(environment, jdbcTemplate, getTagRepository(environment, jdbcTemplate));
    }
}
