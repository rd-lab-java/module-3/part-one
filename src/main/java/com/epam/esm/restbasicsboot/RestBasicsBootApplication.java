package com.epam.esm.restbasicsboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;

@SpringBootApplication
public class RestBasicsBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestBasicsBootApplication.class, args);
	}

}
