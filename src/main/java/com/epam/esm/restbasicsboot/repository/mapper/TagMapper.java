package com.epam.esm.restbasicsboot.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.esm.restbasicsboot.repository.entity.Tag;
import org.springframework.jdbc.core.RowMapper;


public class TagMapper implements RowMapper<Tag>{

	@Override
	public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
		Tag tag = new Tag();
		tag.setId(rs.getLong("id"));
		tag.setName(rs.getString("name"));
		return tag;
	}

}
