create table if not exists gift_certificate (
	id bigint auto_increment primary key,
    description varchar(255),
    price double not null,
    duration int not null,
    create_date datetime,
    last_update_date datetime,
    name varchar(255) unique not null);
    
create table if not exists tag(
	id bigint auto_increment primary key,
    name varchar(255) unique not null
);

create table if not exists gift_certs_tags(
	gift_cert_id bigint,
    tag_id bigint,
    primary key(gift_cert_id, tag_id),
    foreign key(gift_cert_id) references gift_certificate(id) on delete cascade,
    foreign key(tag_id) references tag(id) on delete cascade,
    unique key(gift_cert_id, tag_id)
);